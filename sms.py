from twilio.rest import Client 
import random
import math
from datetime import datetime, timedelta
# TODO: check if 1 hour has passed
class SMS():
    '''A class used to manage sms authentication'''
    def __init__(self, account_sid: str, auth_token: str) -> None:
        self.account_sid = account_sid
        self.auth_token = auth_token
        self.client = Client(account_sid, auth_token)

    def send_token(self, tel_num: str, text: str):
        ''' 
        Send a sms
        ...

        Parameters
        ----------
        tel_num: str, required
            Number to send a sms to (+31612345678)
        text: str, required
            Text that will be send to the number
        '''
        self.client.messages.create(
            body=text,
            from_='+19706496692',
            to=tel_num
        )

    def create_code(self) -> int:
        '''Generate 6 digit code'''
        digits = [i for i in range(0, 10)]
        create_code = ""
        for i in range(6):  
            index = math.floor(random.random() * 10)
            create_code += str(digits[index])
        return int(create_code)
    

    def check_validation(self,time_db) -> str:
        '''Check if the code is valid code is 15 min valid'''
        dt = datetime.strptime(str(time_db), '%Y-%m-%d %H:%M:%S.%f')
        now =datetime.now()
        max_time = dt +timedelta(minutes=15)
        if max_time > now:
            return True
        else:
            return False

