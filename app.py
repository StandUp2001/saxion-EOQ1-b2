from flask import Flask, render_template, url_for, request, redirect
from flask_bootstrap import Bootstrap
from flask_sqlalchemy import SQLAlchemy
import postgresqlite
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin, login_user, LoginManager, current_user, logout_user, login_required
from flask_wtf.csrf import CSRFProtect
from dotenv import load_dotenv
import os
from datetime import datetime
from insert_data import add_data
from sms import SMS


# load local .env
load_dotenv()

#! CHANGE TO TRUE TO MAKE DATABASE
# create database
CREATE_DB = False

# Configure Flask app
app = Flask(__name__)
app.secret_key = os.urandom(32)
app.config['TEMPLATES_AUTO_RELOAD'] = True
Bootstrap(app)

# Configure database
app.config["SQLALCHEMY_DATABASE_URI"] = postgresqlite.get_uri()
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy()
db.init_app(app)


# Configure Login manager
login_manager = LoginManager()
login_manager.login_view = 'home'
login_manager.init_app(app)


# // TODO: create
# ///TODO: Add token table
class Users(db.Model, UserMixin):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    name = db.Column(db.String, nullable=False)
    email = db.Column(db.String, nullable=False)
    phone_number = db.Column(db.String, nullable=False)
    amount_due = db.Column(db.Float, nullable=False)
    is_admin = db.Column(db.Boolean, nullable=False)
    profile_file = db.Column(db.String, nullable=True)
    orders = db.relationship('Orders', back_populates='user_order')
    token = db.relationship('Tokens', back_populates='user_token')


class Orders(db.Model):
    __tablename__ = 'orders'
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    user_order = db.relationship('Users', back_populates='orders')
    timestamp = db.Column(db.DateTime, nullable=False, default=datetime.now())


class Products(db.Model):
    __tablename__ = 'products'
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    name = db.Column(db.String, nullable=True)
    price = db.Column(db.Float, nullable=False)
    amount = db.Column(db.Integer, nullable=False)
    categories = db.relationship('Categories', back_populates='products')


class Order_products(db.Model):
    __tablename__ = 'order_products'
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    order_id = db.Column(db.Integer, db.ForeignKey(
        'orders.id'))
    product_id = db.Column(db.Integer, db.ForeignKey(
        'products.id'))
    amount = db.Column(db.Integer, nullable=False, default=1)


class Categories(db.Model):
    __tablename__ = 'categories'
    name = db.Column(db.String, primary_key=True, nullable=False)
    products = db.relationship('Products', back_populates='categories')
    product_id = db.Column(db.Integer, db.ForeignKey('products.id'))


class Tokens(db.Model):
    __tablename__ = 'tokens'
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    user_token = db.relationship('Users', back_populates='token')
    token = db.Column(db.Integer, nullable=False)
    timestamp = db.Column(db.DateTime, nullable=False, default=datetime.now())


# // TODO: SET IN MODULE
if CREATE_DB:
    with app.app_context():

        db.create_all()

        add_data(app, db, Users, Orders, Categories, Products, Order_products)

    exit()


@login_manager.user_loader
def load_user(user_id):
    return Users.query.get(int(user_id))


@app.route('/', methods=['GET', 'POST'])
def home():
    # * Get all users
    users = Users.query.order_by(Users.name).all()

    if request.method == 'POST':
        user_id = request.form['user_id']
        user = Users.query.get(user_id)
        send_code(user)

        return redirect(url_for('verificate_code', user_id=user_id))

    return render_template('index.html', users=users, hidden_module=False)

@app.route('/verificate_code/<int:user_id>', methods=['GET', 'POST'])
def verificate_code(user_id):
    if request.method == 'POST':
        user = Users.query.get(user_id)

        code = int(request.form['code'])

        print(code, user.token[-1].token)
        if code == user.token[-1].token:
            if validate_code(user, code):
                login_user(user)
                return redirect(url_for('products'))
            else:
                return redirect(url_for('home'))
        else:
            print('code is not valid')
    return render_template('verification.html', user_id=user_id)

@app.route('/logout')
@login_required
def logout():
    logout_user(current_user)
    return redirect(url_for('home'))

@app.route('/products')
def products():
    # * Get all users
    products = Products.query.all()

    return render_template('products.html', products=products, product_len=range(3))

def send_code(user: Users):
    sms = SMS(os.getenv('ACCOUNT_SID'), os.getenv('AUTH_TOKEN'))
    code = sms.create_code()
    sms.send_token(os.getenv('number'), code)

    token = Tokens(user_id=user.id, token=code)
    db.session.add(token)
    db.session.commit()

def validate_code(user: Users, code: int) -> bool:
    sms = SMS(os.getenv('ACCOUNT_SID'), os.getenv('AUTH_TOKEN'))
    token = Tokens.query.filter_by(token=code).first()
    print(code)
    print(sms.check_validation(token.timestamp))
    if sms.check_validation(token.timestamp):
        if token.user_id == user.id:
            return True
    return False

@app.route('/admin')
def admin():
    # * Get all users
    users = Users.query.order_by(Users.name).all()
    one_user = Users.query.get(1)
    return render_template('admin.html', users = users, one_user=one_user)

@app.route('/admin/<id_user>')
def admin_person(id_user):
    # * Get all users
    users = Users.query.order_by(Users.name).all()
    one_user = Users.query.get(id_user)
    if one_user == None:
        one_user = Users.query.get(1)
    order_id = [order.id for order in one_user.orders]
    orderProduct = [Order_products.query.filter_by(order_id = id).first() for id in order_id]
    products  = [Products.query.filter_by(id = n.product_id).first() for n in orderProduct if n != None]
    print(products)
    
    return render_template('admin.html', users=users, one_user=one_user, products=products)

if __name__ == '__main__':
    app.run(debug=True, host='127.0.0.1', port=5002)
