users: name, id, email, phone_number, (photo?), saldo
inventory: item_name, price, id, (description), amount
transactions: id, inventory_ids, user_id


##space
```plantuml

entity user {
    id: int
    --
    name: text
    email: text
    phone_number: int
    amount_due: int
    is_admin: boolean
}

entity order_product {
    order_id: int <<FK>>
    product_id: int <<FK>>
    --
    amount: int
}

entity order {
    id: serial
    --
    user_id: int <<FK>>
    timestamp: timestamp
}

entity product {
    id: serial
    --
    name: text
    price: float
    amount: int
}

entity product_category {
    product_id: int <<FK>>
    category_id: int <<FK>>
}

entity category {
    id: serial
    --
    name: text
}


order ||--|{ order_product

order_product }o--|| product

user ||--o{ order

category ||-o{ product_category

product_category }|-|| product
```