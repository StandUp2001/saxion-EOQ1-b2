import csv


def add_data(app, db, Users, Orders, Categories, Products, Order_products):
    with app.app_context():
        with open('data_csv/users.csv') as filecontent:
            data = list(csv.DictReader(filecontent))

            for row in data:

                row['is_admin'] = row['is_admin'] == "True"


                new_user = Users(name=row['name'], email=row['email'], phone_number=row['phone_number'],
                                 amount_due=row['amount_due'], is_admin=bool(row['is_admin']), profile_file=row['profile_file'])
                db.session.add(new_user)
                db.session.commit()
                print(f'add user {row["name"]}')

        with open('data_csv/orders.csv') as filecontent:
            data = list(csv.DictReader(filecontent))

            for row in data:
                new_user = Orders(
                    user_id=row['user_id'], timestamp=row['timestamp'])
                db.session.add(new_user)
                db.session.commit()

        with open('data_csv/products.csv') as filecontent:
            data = list(csv.DictReader(filecontent))

            for row in data:
                new_product = Products(
                    name=row['name'], price=row['price'], amount=row['amount'])
                db.session.add(new_product)
                db.session.commit()

        with open('data_csv/order_products.csv') as filecontent:
            data = list(csv.DictReader(filecontent))

            for row in data:
                new_user = Order_products(
                    order_id=row['order_id'], product_id=row['product_id'], amount=row['amount'])
                db.session.add(new_user)
                db.session.commit()



        with open('data_csv/categories.csv') as filecontent:
            data = list(csv.DictReader(filecontent))

            for row in data:
                new_user = Categories(name=row['name'])
                db.session.add(new_user)
                db.session.commit()
